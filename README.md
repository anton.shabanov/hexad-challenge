# Hexad Frontend App

## Video Example

Please follow this link to see the app in action: https://monosnap.com/file/2cRdChXFFuRLObHM3QxRokZ9H0dd1N

## Installation

Make sure you're running node 11.12.0 or above.

```
$ cd project-dir
$ git clone repo-url .
$ yarn # to install packages
$ yarn dev # to start dev server
```

## Tests, Linters, Prettier

By default tests, linter & prettier are included inside the pre-commit hook, but if you want to run them manually please use these commands:

```
$ yarn pretty # to run prettier
$ yarn lint --fix # to run linter
$ yarn test # to run tests
$ yarn stylelint # to run styles linter
```

## Further improvement

The next imporvement that could be done for the app is adding virtual scroll to the list just not to render large amount of nodes directly to dom (but sorry, didn't have enough time to implement it since had limited time range, but shouldn't be that hard via existing package or in-memory memoized scroll handing with futher rendering).
