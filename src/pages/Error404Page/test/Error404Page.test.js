import React from 'react';
import { mount } from 'enzyme';
import Error404Page from 'src/pages/Error404Page';

describe('Pages', () => {
  describe('Error404Page', () => {
    it('renders 404 error', () => {
      const wrapper = mount(<Error404Page />);
      const text = wrapper
        .find('h1')
        .first()
        .text();
      expect(text).toBe('404. Page Not Found.');
    });
  });
});
