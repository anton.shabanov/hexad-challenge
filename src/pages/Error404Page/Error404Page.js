import React, { Fragment } from 'react';
import Meta from 'src/components/Meta';
import { PageTitle } from 'src/components/Page';

const Error404Page = () => (
  <Fragment>
    <Meta title="Page Not Found" />
    <PageTitle center>404. Page Not Found.</PageTitle>
  </Fragment>
);

export default React.memo(Error404Page);
