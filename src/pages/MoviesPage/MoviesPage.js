import React from 'react';
import { PageTitle, PageContainer, PageContent } from 'src/components/Page';
import MoviesList from 'src/components/MoviesList';
import Button from 'src/components/Button';
import usePage from './usePage';

const MoviesPage = () => {
  const [{ list, simulating }, { handleRate, handleSimulate }] = usePage();
  const buttonText = simulating ? 'Stop simulation' : 'Simulate';
  return (
    <PageContainer>
      <PageTitle inline>
        Movies
        <Button onClick={handleSimulate}>{buttonText}</Button>
      </PageTitle>
      <PageContent spaced>
        <MoviesList spaced items={list} onMovieRate={handleRate} />
      </PageContent>
    </PageContainer>
  );
};

export default React.memo(MoviesPage);
