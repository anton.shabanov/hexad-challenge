import { createSelector } from 'reselect';

const moviesSelector = state => state.movies;

export default createSelector(
  moviesSelector,
  movies => {
    return [...movies].sort(
      (a, b) => b.reviews.rating - a.reviews.rating || a.title - b.title,
    );
  },
);
