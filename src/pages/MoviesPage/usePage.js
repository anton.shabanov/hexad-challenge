import { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { rateMovie } from 'src/actionCreators/movies';
import { notification } from 'src/components/Notification';
import useSimulator from './useSimulator';
import moviesSelector from './selectors';

export default () => {
  const list = useSelector(moviesSelector);
  const dispatch = useDispatch();

  const handleRate = useCallback(
    (movie, rating) => {
      dispatch(rateMovie(movie, rating));
      notification.success(
        `Thanks. ${movie.title} has been rated with ${rating} stars!`,
      );
    },
    [dispatch],
  );

  const [simulating, handleSimulate] = useSimulator();

  return [{ list, simulating }, { handleRate, handleSimulate }];
};
