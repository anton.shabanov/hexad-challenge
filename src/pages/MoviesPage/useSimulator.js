import { useState, useCallback, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { rateRandomMovie } from 'src/actionCreators/movies';
import { random } from 'src/utils/core/number';

export default () => {
  const dispatch = useDispatch();
  const [simulating, setSimulating] = useState(false);
  const timer = useRef();

  const handleSimulate = useCallback(() => {
    if (simulating) {
      clearInterval(timer.current);
    } else {
      const interval = random(5000, 1000);

      timer.current = setInterval(() => {
        const rating = random(10, 1);
        dispatch(rateRandomMovie(rating));
      }, interval);
    }

    setSimulating(!simulating);
  }, [simulating]);

  return [simulating, handleSimulate];
};
