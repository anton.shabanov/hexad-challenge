import styled from 'astroturf';

const content = styled('div')`
  position: relative;
`;

const layout = styled('div')`
  position: relative;
  padding: 50px 0;
`;

export default {
  Content: content,
  Layout: layout,
};
