import React from 'react';
import PropTypes from 'prop-types';
import Meta from 'src/components/Meta';
import ErrorBoundary from 'src/components/ErrorBoundary';
import Notification from 'src/components/Notification';
import Styled from './PublicLayout.styles';

const PublicLayout = ({ children }) => (
  <Styled.Layout>
    <Meta />
    <ErrorBoundary>
      <Styled.Content>{children}</Styled.Content>
    </ErrorBoundary>
    <Notification />
  </Styled.Layout>
);

PublicLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.string,
    PropTypes.node,
  ]).isRequired,
};

export default React.memo(PublicLayout);
