import { createStore, applyMiddleware, compose } from 'redux';
import createReducer from 'src/reducers';

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default initialState => {
  const enhancers = composeEnhancers(applyMiddleware());
  const store = createStore(createReducer(), initialState, enhancers);

  // in case of hot reload...
  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};
