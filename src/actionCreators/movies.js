import { create } from './utils';

export const rateMovie = create(
  (movie, rating) => ({
    payload: { movie, rating },
  }),
  'src/actionCreators/movies/RATE_MOVIE',
);

export const rateRandomMovie = create(
  rating => ({
    payload: { rating },
  }),
  'src/actionCreators/movies/RATE_RANDOM_MOVIE',
);
