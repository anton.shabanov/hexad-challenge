// eslint-disable-next-line import/prefer-default-export
export const create = (fn, name) => {
  const newFn = (...args) => {
    const result = fn(...args);
    return {
      ...result,
      type: name,
    };
  };
  newFn.toString = () => name;
  return newFn;
};
