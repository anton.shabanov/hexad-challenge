import React from 'react';
import { Switch } from 'react-router-dom';
import PublicLayout from 'src/layouts/PublicLayout';
import Error404Page from 'src/pages/Error404Page';
import Route from 'src/components/Route';
import * as ROUTE from './routes';

// eslint-disable-next-line react/display-name
export default () => (
  <Switch>
    <Route
      exact
      async
      path={ROUTE.HOME}
      component={() => import('src/pages/MoviesPage')}
      layout={PublicLayout}
    />

    <Route layout={PublicLayout} component={Error404Page} />
  </Switch>
);
