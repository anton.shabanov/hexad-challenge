// constants
export const HOME = '/';
export const MOVIES = '/movies';
export const MOVIE = '/movies/:id';

// helpers
export const makeMovieUrl = (id = 'new') => MOVIE.replace(':id', id);
