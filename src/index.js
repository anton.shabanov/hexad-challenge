import React from 'react';
import ReactDOM from 'react-dom';
import App from 'src/pages/App';

// store
import configureStore from 'src/store/configureStore';

// styles
import 'src/styles/app.css';

const APP_ROOT = document.getElementById('app');
const store = configureStore();

ReactDOM.render(<App store={store} />, APP_ROOT);
