import { random } from 'src/utils/core/number';

describe('Utils', () => {
  describe('Core', () => {
    describe('Number', () => {
      it('can get random number without min limit', () => {
        const number = random(10);
        expect(number).toBeLessThan(10);
      });

      it('can get random number with min limit', () => {
        const number = random(10, 2);
        expect(number).toBeLessThanOrEqual(10);
        expect(number).toBeGreaterThanOrEqual(2);
      });
    });
  });
});
