import { random, range } from 'src/utils/core/array';

describe('Utils', () => {
  describe('Core', () => {
    describe('Array', () => {
      it('can get random element from array', () => {
        const array = [1, 2, 3, 4];
        const element = random(array);
        expect(array.indexOf(element)).not.toBe(-1);
      });

      it('can generate give range', () => {
        const array = range(1, 10);
        expect(array.indexOf(1)).not.toBe(-1);
      });
    });
  });
});
