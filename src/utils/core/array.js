import { random as randomNumber } from './number';

export const random = arr => arr[randomNumber(arr.length)];

export const range = (start, end) => {
  if (start === end) return [start];
  return [start, ...range(start + 1, end)];
};
