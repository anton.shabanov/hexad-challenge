// eslint-disable-next-line import/prefer-default-export
export const random = (max, min = 0) => {
  const number = Math.floor(Math.random() * max) + min;
  return (number >= max && max) || number;
};
