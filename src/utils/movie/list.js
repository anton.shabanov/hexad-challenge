// eslint-disable-next-line import/prefer-default-export
export const update = (movie, list) => {
  const without = list.filter(m => m.id !== movie.id);
  return [movie, ...without];
};
