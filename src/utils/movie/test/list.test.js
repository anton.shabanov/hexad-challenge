import { update } from 'src/utils/movie/list';
import movies from 'src/utils/data/movies';

describe('Utils', () => {
  describe('Core', () => {
    describe('Movie', () => {
      it('can update list with new item', () => {
        const lastMovie = movies[movies.length - 1];
        const updatedMovie = { ...lastMovie, title: 'Updated Baz' };
        const newList = update(updatedMovie, movies);

        expect(newList.length).toBe(movies.length);
        expect(newList[0]).not.toEqual(lastMovie);
        expect(newList[0]).toEqual(updatedMovie);
      });
    });
  });
});
