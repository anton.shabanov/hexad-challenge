import { calculateRating, applyRating } from 'src/utils/movie/rating';

describe('Utils', () => {
  describe('Core', () => {
    describe('Movie', () => {
      const expectedReviews = { rating: 8.5, count: 4 };
      let movie = null;

      beforeEach(() => {
        movie = { id: 1, title: 'Test One', reviews: { rating: 8, count: 3 } };
      });

      it('can calculate new rating based on reviews', () => {
        const newRating = calculateRating(movie.reviews, 10); // assuming rating = (8 * 3 + 10) / 4
        expect(newRating).toBe(expectedReviews.rating);
      });

      it('could apply rating without mutating initial object', () => {
        const newMovie = applyRating(movie, 10);
        expect(newMovie.reviews).toEqual(expectedReviews);
        expect(movie === newMovie).toBeFalsy();
      });
    });
  });
});
