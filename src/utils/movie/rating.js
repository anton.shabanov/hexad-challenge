export const calculateRating = (reviews, rating) => {
  const newCount = reviews.count + 1;
  return +((reviews.rating * reviews.count + rating) / newCount).toFixed(2);
};

export const applyRating = (movie, rating) => {
  const { reviews } = movie;

  const newCount = reviews.count + 1;
  const newRating = calculateRating(reviews, rating);

  return {
    ...movie,
    reviews: { count: newCount, rating: newRating },
  };
};
