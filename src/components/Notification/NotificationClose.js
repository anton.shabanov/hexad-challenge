import React from 'react';
import PropTypes from 'prop-types';
import Styled from './Notification.styles';

const NotificationClose = ({ closeToast }) => (
  <Styled.Close onClick={closeToast}>X</Styled.Close>
);

NotificationClose.propTypes = {
  closeToast: PropTypes.func.isRequired,
};

export default React.memo(NotificationClose);
