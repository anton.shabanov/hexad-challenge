import React from 'react';
import { ToastContainer } from 'react-toastify';
import NotificationClose from './NotificationClose';

const Notification = ({ ...props }) => (
  <ToastContainer
    hideProgressBar
    {...props}
    closeButton={<NotificationClose />}
  />
);

export default React.memo(Notification);
