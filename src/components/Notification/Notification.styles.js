import styled from 'astroturf';

const close = styled('span')`
  font-size: 1.6rem;
`;

export default { Close: close };
