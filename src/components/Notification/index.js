import Notification from './Notification';
import { notification } from './utils';

export { notification };
export default Notification;
