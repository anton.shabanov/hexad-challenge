import styled from 'astroturf';

const pageTitle = styled('h1')`
  font-size: 3.2rem;
  font-weight: var(--ph-font-weight-bold);
  color: var(--dune);
  line-height: 1.1875;
  margin: 0;
  padding: 0;

  &.center {
    text-align: center;
  }

  &.inline {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }
`;

const pageContainer = styled('div')`
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
  background-color: var(--white);
  padding: 32px 46px 31px 38px;
  max-width: var(--grid-width-default);
  margin: 0 auto;

  &.fullWidth {
    max-width: 100%;
  }
`;

const pageContent = styled('div')`
  position: relative;

  &.spaced {
    margin-top: 30px;
  }
`;

export default {
  PageTitle: pageTitle,
  PageContainer: pageContainer,
  PageContent: pageContent,
};
