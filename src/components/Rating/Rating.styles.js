import styled from 'astroturf';

const rating = styled('div')`
  display: flex;
  flex-direction: row;
`;

const star = styled('div')`
  font-size: 1.2rem;
  color: var(--brownish-grey);

  & + & {
    padding-left: 3px;
  }

  &.selected,
  &:hover {
    color: var(--casablanca);
  }
`;

export default {
  Rating: rating,
  Star: star,
};
