import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { range } from 'src/utils/core/array';
import Star from './Star';
import Styled from './Rating.styles';

const stars = range(1, 10);

const Rating = ({ onSelect, value, ...rest }) => {
  const rating = Math.round(value);
  const [hover, setHover] = useState(rating);
  return (
    <Styled.Rating {...rest}>
      {stars.map(s => (
        <Star
          key={s}
          selected={s <= hover}
          onClick={() => onSelect(s)}
          onMouseEnter={() => setHover(s)}
          onMouseLeave={() => setHover(rating)}
        />
      ))}
    </Styled.Rating>
  );
};

Rating.propTypes = {
  value: PropTypes.number,
  onSelect: PropTypes.func.isRequired,
};

Rating.defaultProps = {
  value: null,
};

export default React.memo(Rating);
