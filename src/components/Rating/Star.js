import React from 'react';
import Styled from './Rating.styles';

const Star = props => <Styled.Star {...props} className="fa fa-star" />;

export default React.memo(Star);
