import React from 'react';
import { shallow } from 'enzyme';
import Rating from 'src/components/Rating';
import Star from 'src/components/Rating/Star';

describe('Components', () => {
  describe('Rating', () => {
    const onSelectHandler = jest.fn();

    it('should render 10 stars', () => {
      const wrapper = shallow(<Rating />);
      expect(wrapper.find(Star).length).toBe(10);
    });

    it('should be able to auto select starts when value passed', () => {
      const wrapper = shallow(<Rating value={8} />);
      expect(wrapper.find({ selected: true }).length).toBe(8);
    });

    it('should be able react on star select', () => {
      const wrapper = shallow(<Rating value={6} onSelect={onSelectHandler} />);
      wrapper
        .find(Star)
        .last()
        .simulate('click');

      expect(wrapper.find({ selected: true }).length).toBe(6);
      expect(onSelectHandler).not.toHaveBeenCalledWith(6);
      expect(onSelectHandler).toHaveBeenCalledWith(10);
    });
  });
});
