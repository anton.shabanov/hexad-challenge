import React from 'react';
import Helmet from 'react-helmet';

const Meta = props => (
  <Helmet
    defaultTitle="Movies App | Hexad"
    titleTemplate="%s | Hexad"
    {...props}
  >
    <meta name="description" content="Movies Web Application" />
    <meta name="robots" content="noindex" />
  </Helmet>
);

export default React.memo(Meta);
