import styled from 'astroturf';
import Rating from 'src/components/Rating';

const list = styled('div')`
  display: flex;
  flex-direction: column;

  &.spaced {
    margin: 0 -20px;
  }
`;

const movie = styled('div')`
  display: flex;
  flex-direction: row;
  padding: 17px 20px 20px;
  border: solid 1px transparent;
  cursor: pointer;

  &:hover {
    border-color: var(--mercury);
    background-color: var(--alabaster);
  }
`;

const movieItem = styled('div')`
  flex: 1;
  font-size: 1.4rem;
  line-height: 1.43;
  color: rgba(#332d29, 0.7);

  &.info {
    flex: 0 0 30%;
  }

  &.rating {
    flex: 0 0 20%;
  }

  & + & {
    margin-left: 20px;
  }
`;

const movieTitle = styled('span')`
  font-size: 1.6rem;
  line-height: 1.25;
  color: var(--dune);
  margin-right: 10px;
  font-weight: var(--ph-font-weight-bold);
`;

const movieYear = styled('span')`
  display: inline-block;
  border-radius: 20px;
  border: solid 1px var(--alto);
  background-color: var(--white);
  font-size: 1rem;
  color: var(--dune);
  padding: 1px 8px;
`;

const movieRating = styled('div')`
  font-size: 1.5rem;
  font-weight: var(--ph-font-weight-bold);

  &.green {
    color: var(--dark-pastel-green);
  }

  &.red {
    color: var(--vermillion);
  }
`;

const movieReviews = styled('div')`
  margin-top: 3px;
  font-size: 1.1rem;
`;

const movieGenre = styled('div')`
  font-size: 1.3rem;
  font-weight: var(--ph-font-weight-bold);

  & + & {
    margin-top: 1px;
  }
`;

const movieStarts = styled(Rating)`
  margin-top: 5px;
`;

export default {
  List: list,
  Movie: movie,
  MovieItem: movieItem,
  MovieTitle: movieTitle,
  MovieYear: movieYear,
  MovieRating: movieRating,
  MovieReviews: movieReviews,
  MovieGenre: movieGenre,
  MovieStarts: movieStarts,
};
