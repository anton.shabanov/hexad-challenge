import React from 'react';
import PropTypes from 'prop-types';
import Styled from './MoviesList.styles';

const GOOD_RATING = 8.3;

const Movie = ({ movie, onRate }) => {
  const hasGoodRating = movie.reviews.rating > GOOD_RATING;
  return (
    <Styled.Movie>
      <Styled.MovieItem info>
        <Styled.MovieTitle>{movie.title}</Styled.MovieTitle>

        <Styled.MovieYear>{movie.year}</Styled.MovieYear>
      </Styled.MovieItem>

      <Styled.MovieItem>{movie.releaseDate}</Styled.MovieItem>

      <Styled.MovieItem>
        {movie.genres.map(genre => (
          <Styled.MovieGenre key={genre}>{genre}</Styled.MovieGenre>
        ))}
      </Styled.MovieItem>

      <Styled.MovieItem rating>
        <Styled.MovieRating green={hasGoodRating} red={!hasGoodRating}>
          {movie.reviews.rating}
        </Styled.MovieRating>

        <Styled.MovieReviews>
          based on {movie.reviews.count} reviews
        </Styled.MovieReviews>

        <Styled.MovieStarts
          value={movie.reviews.rating}
          onSelect={r => onRate(movie, r)}
        />
      </Styled.MovieItem>
    </Styled.Movie>
  );
};

Movie.propTypes = {
  movie: PropTypes.shape({}).isRequired,
  onRate: PropTypes.func.isRequired,
};

export default React.memo(Movie);
