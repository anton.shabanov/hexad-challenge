import React from 'react';
import { shallow } from 'enzyme';
import MoviesList from 'src/components/MoviesList';
import Movie from 'src/components/MoviesList/Movie';
import movies from 'src/utils/data/movies';

describe('Components', () => {
  describe('MoviesList', () => {
    it('should render movies list', () => {
      const wrapper = shallow(<MoviesList items={movies} />);
      expect(wrapper.find(Movie).length).toBe(movies.length);
    });
  });
});
