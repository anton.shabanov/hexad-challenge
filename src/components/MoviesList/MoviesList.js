import React from 'react';
import PropTypes from 'prop-types';
import Movie from './Movie';
import Styled from './MoviesList.styles';

const MoviesList = ({ items, onMovieRate, ...rest }) => (
  <Styled.List {...rest}>
    {items.map(item => (
      <Movie key={item.id} movie={item} onRate={onMovieRate} />
    ))}
  </Styled.List>
);

MoviesList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  onMovieRate: PropTypes.func.isRequired,
};

export default React.memo(MoviesList);
