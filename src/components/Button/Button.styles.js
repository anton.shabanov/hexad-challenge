import styled from 'astroturf';

const button = styled('button')`
  border-radius: 20px;
  padding: 10px 20px;
  background-image: linear-gradient(to left, #f26f23, var(--casablanca));
  font-size: 12px;
  line-height: 1.67;
  letter-spacing: 0.9px;
  color: var(--white);
  border: none;
  cursor: pointer;
  text-transform: uppercase;
  font-weight: var(--ph-font-weight-bold);
  outline: none;

  &:hover {
    opacity: 0.9;
  }
`;

export default {
  Button: button,
};
