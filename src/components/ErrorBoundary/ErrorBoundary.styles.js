import styled from 'astroturf';

const stack = styled('pre')`
  font-size: 14px;
  white-space: pre-wrap;
`;

export default { Stack: stack };
