import React, { Component } from 'react';
import PropTypes from 'prop-types';
import logger from 'src/utils/logger';
import { PageTitle } from 'src/components/Page';
import Styled from './ErrorBoundary.styles';

class ErrorBoundary extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string,
      PropTypes.node,
    ]).isRequired,
    className: PropTypes.string,
  };

  static defaultProps = {
    className: null,
  };

  state = { error: null, errorInfo: null };

  static getDerivedStateFromError(error) {
    logger.info(error);
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error, errorInfo });
  }

  render() {
    const { children, className } = this.props;
    const { errorInfo, error } = this.state;
    if (!errorInfo) return children;
    return (
      <div className={className}>
        <PageTitle>Error Occured</PageTitle>
        <Styled.Stack>
          {error && error.toString()}
          <br />
          {errorInfo.componentStack}
        </Styled.Stack>
      </div>
    );
  }
}

export default ErrorBoundary;
