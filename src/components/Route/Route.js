import React from 'react';
import PropTypes from 'prop-types';
import { Route as ReactRoute } from 'react-router-dom';
import loadable from '@loadable/component';

const Route = ({ component, layout: Layout, async: asyncRouts, ...rest }) => {
  const Component = asyncRouts ? loadable(component) : component;
  return (
    <ReactRoute
      {...rest}
      render={props => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
};

Route.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.shape({})])
    .isRequired,
  layout: PropTypes.oneOfType([PropTypes.func, PropTypes.shape({})]).isRequired,
  async: PropTypes.bool,
};

Route.defaultProps = {
  async: false,
};

export default Route;
