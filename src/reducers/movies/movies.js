import { rateMovie, rateRandomMovie } from 'src/actionCreators/movies';
import { updateList, applyRating } from 'src/utils/movie';
import { random } from 'src/utils/core/array';
import data from 'src/utils/data/movies';

const initialState = data;

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case rateMovie.toString(): {
      const { movie, rating } = payload;
      const newMovie = applyRating(movie, rating);
      return updateList(newMovie, state);
    }
    case rateRandomMovie.toString(): {
      const { rating } = payload;
      const newMovie = applyRating(random(state), rating);
      return updateList(newMovie, state);
    }
    default:
      return state;
  }
};
