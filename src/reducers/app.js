const initialState = { version: VERSION };

export default (state = initialState, { type }) => {
  switch (type) {
    default:
      return state;
  }
};
